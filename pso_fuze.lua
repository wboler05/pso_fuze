--[[

Copyright 2020 William M. Boler

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--]]

recipe_name = "PSO Fuze"
recipe_author = "wboler"
recipe_version = "1.0.0"

-- Some fun functions

math.randomseed(os.time())

function random_range(min_val, max_val)
  return math.random()*(max_val - min_val) + min_val 
end

function error_msg(func_name, msg)
  --print(debug.traceback())
  error("Error:: "..func_name.."::"..msg)
end

function clamp(x, min_x, max_x)
  local y = x
  if y < min_x then
    y = min_x
  end
  if y > max_x then
    y = max_x
  end
  return y
end

-- End the fun functions

-- *** PSO Functions *** --

MIN_FITNESS=-1e309

pso_params = {}
pso_params.population=9      -- number of particles
pso_params.neighbors=3       -- number of neighbors
pso_params.max_iterations=0  -- max iterations to run, 0 for infinite
pso_params.min_iterations=100 -- minimum iterations before consider stopping
pso_params.min_delta=1E-3    -- minimum score delta before determining end of growth
pso_params.window_size=10   -- Size
pso_params.min_energy=0
pso_params.discount_fitness=1E-8 -- Subtracts from each pb and lb score each epoch


particle_params = {}
particle_params.I = 0.75    -- Inertia Weight
particle_params.C1 = 2.49   -- Cognitive Constant
particle_params.C2 = 2.49   -- Social Constant
particle_params.C3 = 1.0    -- Noise Constant
particle_params.min_x = {}
particle_params.max_x = {}
particle_params.max_v = {}

-- Table of Particles
particles = {}
-- Global Best Index Tracker
global_best_idx = nil

function PopulateParticles()

  for i=1,pso_params.population do
    particle = {}
    particle.x = {}
    particle.v = {}
    particle.fitness = MIN_FITNESS
    particle.x_pb = {}
    particle.fitness_pb = MIN_FITNESS
    particle.x_lb = {}
    particle.fitness_lb = MIN_FITNESS
    particles[i] = particle
  end

end

function get_dimension_size()
  return #particle_params.min_x
end

function check_dimensions()

  dimension_size = get_dimension_size()
  if #particle_params.max_x ~= dimension_size then
    error_msg("check_dimensions", "Min and Max particle_params do not match")
  end
  if #particle_params.max_v ~= dimension_size then
    error_msg("check_dimensions", "Max velocity not set correctly")
  end
  if #particle_params.min_x < 1 or #particle_params.max_x < 1 then
    error_msg("check_dimensions", "Need to set Min and Max in particle_params")
  end
  for i=1,dimension_size do
      if particle_params.min_x[i] == nil then
        error_msg("check_dimensions", "Min Not Set")
      end
      if particle_params.max_x[i] == nil then
        error_msg("check_dimensions", "Max Not Set")
      end
      if particle_params.max_v[i] == nil then
        error_msg("check_dimensions", "Max Velocity Not Set")
      end
  end

end

function RandomizeParticleState(i)

  if i < 0 or i > #particles then
    error_msg("RandomizeParticleState", "Particle index is out of bounds")
  end
  local particle = particles[i]
  for j=1,dimension_size do
    particle.x[j] = random_range(particle_params.min_x[j], particle_params.max_x[j])
    particle.v[j] = 0
    particle.x_pb[j] = particle.x[j]
    particle.x_lb[j] = particle.x[j]
  end
  particle.fitness = MIN_FITNESS
  particle.fitness_pb = MIN_FITNESS
  particle.fitness_lb = MIN_FITNESS
  
end

function RandomizeParticleStates()
  
  -- A bit of error checking to start off with
  check_dimensions()

  dimension_size = get_dimension_size()
  print("Dimension: "..dimension_size)

  for i=1,pso_params.population do
    RandomizeParticleState(i)
  end

end

function FlyParticles()

  dimension_size = get_dimension_size()

  local avg_energy = 0

  for i=1,#particles do
    local particle = particles[i]
    local dim_energy = 0
    for j=1,dimension_size do
    
      -- Calculate Particle Segments
      local inertia = particle_params.I * particle.v[j]
      local c1 = particle_params.C1 * random_range(0.5, 1.0) * (particle.x_pb[j] - particle.x[j])
      local c2 = particle_params.C2 * random_range(0.5, 1.0) * (particle.x_lb[j] - particle.x[j])
      particle.v[j] = inertia + c1 + c2
      
      -- Determine Max Velocity Cuttoff (Important for stability)
      local min_v = -particle_params.max_v[i]
      local max_v =  particle_params.max_v[i]
      particle.v[j] = clamp(particle.v[j], min_v, max_v)
      
      -- Update position with velocity
      particle.x[j] = particle.x[j] + particle.v[j]
      
      -- Restrict position boundaries
      particle.x[j] = clamp(particle.x[j], particle_params.min_x[j], particle_params.max_x[j])
      
      dim_energy = dim_energy + particle.v[j]
    
    end
    
    dim_energy = dim_energy / dimension_size
    avg_energy = avg_energy + dim_energy
  
  end
  
  avg_energy = avg_energy / #particles
  return avg_energy

end

function UpdatePersonalBest()

  -- For each particle
  for i=1,#particles do
    local particle = particles[i]
    -- We want to find if their fitness has grown
    if particle.fitness >= particle.fitness_pb then
      -- If so, we update our personal best fitness
      particle.fitness_pb = particle.fitness
      -- As well as the personal best position
      for j=1,#particle.x do
        particle.x_pb[j] = particle.x[j]
      end
    end
  end

end

function UpdateLocalBest()

  local half_neighbor = math.floor(pso_params.neighbors / 2)
  
  -- For each particle, we will examine the assigned neighbors
  for i=1,#particles do
    local particle = particles[i]
    -- We will track the best index and fitness to save computational cost
    local best_neighbor_idx = -1
    local best_neighbor_fitness = MIN_FITNESS
    -- For each neighbor
    for j=1,pso_params.neighbors do
      -- We calculate the new neighbor ID, left and right, including self
      local neighbor_idx = (#particles+i+j-half_neighbor-2) % #particles + 1
      n_particle = particles[neighbor_idx]
      -- If the neighbor's personal best is better than our local best
      if n_particle.fitness_pb >= best_neighbor_fitness then
        -- We update the that we've found something better
        best_neighbor_fitness = n_particle.fitness_pb
        best_neighbor_idx = neighbor_idx
      end
    
    end -- end each neighbor
    
    -- If the best neighbor ID has been updated, we'll assign the new local best
    if best_neighbor_idx > 0 then
      n_particle = particles[best_neighbor_idx]
      particle.fitness_lb = n_particle.fitness_pb
      for j=1,#particle.x do
        particle.x_lb[j] = n_particle.x_pb[j]
      end 
    end -- end best neighbor idx
  
  end -- end each particle

end

function UpdateGlobalBest()

  local global_best_fitness = MIN_FITNESS
  for i=1,#particles do
  
    local particle = particles[i]
    if particle.fitness_pb >= global_best_fitness then
      global_best_fitness = particle.fitness_pb
      global_best_idx = i
    end
  
  end

end

function EvaluateFitness(fitness_function)
  for i=1,#particles do
    local particle = particles[i]
    particle.fitness = fitness_function(particle.x, i)
  end
end

function DiscountFitness()

  --[[
    Non-Determinstic Fitness: 
    For when the fitness is expected to be non-deterministic, meaning 
    that the same state does not return the same fitness value every 
    time, use this discount value to allow each particle to slowly 
    forget the remembered best locations. 
    This prevents the solution space from becoming stale over time. 
    
    pso_params.discount_fitness = 0  when states are deterministic
    
  --]]
  
  if pso_params.discount_fitness == 0 then
    return
  else
    print("Discounting particles' memory by ", pso_params.discount_fitness)
    for i=1,#particles do
      local particle = particles[i]
      if particle.fitness_pb > MIN_FITNESS then
        particle.fitness_pb = particle.fitness_pb - pso_params.discount_fitness
      end
      if particle.fitness_lb > MIN_FITNESS then
        particle.fitness_lb = particle.fitness_lb - pso_params.discount_fitness
      end
    end
  end

end

function PSO(fitness_function, terminate_function)
  local epochs = 0
  local delta_score = 0.
  local window = {}
  
  if pso_params.window_size < 2 then
    error_msg("PSO", "Window size setting is less than 2")
  end
  
  PopulateParticles()
  RandomizeParticleStates()
  
  while true do
    EvaluateFitness(fitness_function)
    UpdatePersonalBest()
    UpdateLocalBest()
    UpdateGlobalBest()
    local avg_energy = FlyParticles()
    window[#window + 1] = particles[global_best_idx].fitness_pb
    if #window > 1 then
      if #window > pso_params.window_size then
        table.remove(window, 1)
      end
      delta_score = (window[#window] - window[1]) / #window
      if epochs >= pso_params.min_iterations then
        if delta_score < pso_params.min_delta then
          print("Minimum Delta Met: "..delta_score.." < "..pso_params.min_delta)
          break
        end
        if avg_energy < pso_params.min_energy then
          print("Minimum energy met: ", avg_energy.." < "..pso_params.min_energy)
          break
        end
      end
    end
    if pso_params.max_iterations > 0 and epochs >= pso_params.max_iterations then
      print("Maximum Iterations Reached: "..epochs)
      break
    end
    if terminate_function then
      if terminate_function(particles[global_best_idx].x_pb) then
        print("Custom terminate condition met")
        break
      end
    end
    
    print(" - Epoch("..epochs.."): Score: "..window[#window].." Delta: "..delta_score)
    
    DiscountFitness()
    
    epochs = epochs + 1
    
  end
  
  print("PSO Complete")
  
  return particles[global_best_idx]

end

-- *** End PSO Functions *** -- 


-- Example Usage --

function zero_fitness_function(x, idx)
  -- Approx. for the sum of vector x to be zero. 

  fitness = 0
  for i=1,#x do
    --fitness = fitness + math.abs(x[i])
    fitness = fitness + math.pow(x[i], 2)
  end
  
  return -math.sqrt(fitness/#x)

end

function zero_terminate_function(x)
 -- Checks the max of any element in vector x and 
 -- returns true of under a minimum floor. 

  max_val = 0
  for i=1,#x do
    max_val = math.max(max_val, math.abs(x[i]))
  end
  
  if max_val < 0.05 then
    return true
  else
    return false
  end

end


function Test()
  -- Solves PSO for Zero Fitness Function in finite time
  
  local dimensions = 3
  for i=1,dimensions do
    particle_params.min_x[i] = -1.0
    particle_params.max_x[i] =  1.0
  end
  
  local solution = PSO(zero_fitness_function, zero_terminate_function)
  --local solution = PSO(zero_fitness_function)
  print("Solution ::", solution.fitness_pb)
  for i=1,#solution.x do
    print(" -("..i..")- :: "..solution.x_pb[i])
  end

end

-- End Example Usage --


-- ******************************************************** --
-- **************** Begin Foldit Script ******************* --
-- ******************************************************** --


RESTORE_BEST_SLOT=3
RESTORE_BUFFER_SLOT=5
MIN_SCORE_DELTA = 1E-3

WIGGLE_REDUCE_SCORE = 500.0

BAD_GENERATION_EPOCHS = 5

-- Recipe Utility Functions

function Score()

  return current.GetScore()

end

function almost_equals(a,b,delta)

  if not delta then
    return math.abs(a-b) < MIN_SCORE_DELTA
  else
    return math.abs(a-b) < delta
  end

end

function wiggle_complete(delta, debug_flag)
  local start_score = Score()
  local last_score = start_score
  while true do
  
    structure.WiggleAll(5, true, true)
    local new_score = Score()
    if delta then
      if almost_equals(new_score, last_score, delta) then
        break
      else
        last_score = new_score
      end
    else
      if almost_equals(new_score, last_score) then
        break
      else
        last_score = new_score
      end
    end
  end
  
  if debug_flag then
    print("Wiggle Score Change: "..(Score() - start_score))
  end
  
end

function wiggle_reduce(delta)

  local start_score = Score()
  local last_score = start_score
  while true do
    structure.WiggleAll(2, true, true)
    local new_score = Score()
    if new_score < start_score - delta then
      break
    elseif almost_equals(new_score, last_score, 0.5) then
      break
    end
    last_score = new_score
  end

end

function shake_once(debug_flag)

  local start_score = Score()
  structure.ShakeSidechainsAll(1)
  if debug_flag then
    print("Shake Once Score Changed: "..(Score() - start_score))
  end

end

function mutate_once(debug_flag)

  local start_score = Score()
  structure.MutateSidechainsAll (1)
  if debug_flag then
    print("Mutate Once Score Changed: "..(Score() - start_score))
  end

end

function round(val)
  return math.floor(val + 0.5)
end

-- Recipe PSO Setup Functions

MAX_SEGMENTS = 5
SEGMENT_SIZE = 4
MAX_WIGGLE_ITERATIONS = 50

-- The PSO state contains information for which order to execute
-- shake and mutate operations.  Clashing importance is first
-- lowered, then shake/mutate is executed.  The choice to determine
-- which order, or whether to execute an operation at all, is 
-- selected by the PSO. 
SHAKE_MUTATE_DEFINITION = {}
SHAKE_MUTATE_DEFINITION[0] = "Disable Both"
SHAKE_MUTATE_DEFINITION[1] = "Disable Mutate"
SHAKE_MUTATE_DEFINITION[2] = "Disable Shake"
SHAKE_MUTATE_DEFINITION[3] = "Shake First"
SHAKE_MUTATE_DEFINITION[4] = "Mutate First"

function calculate_state_size()
  -- [ Enable, CI, SHAKE/MUTATE ORDER, Wiggle Iterations ] * MAX_SEGMENTS
  return MAX_SEGMENTS * SEGMENT_SIZE
end

function sm_def_from_pso(raw_pso_val)

  local sm_idx = clamp(round(raw_pso_val), 0, 4)
  return SHAKE_MUTATE_DEFINITION[sm_idx]

end

function get_structure_from_state(pso_state)

  assert(calculate_state_size() == #pso_state)

  local structure_list = {}
  for i=1,MAX_SEGMENTS do
    local idx = (i-1) * SEGMENT_SIZE + 1
    local entry = {}
    entry.enable_flag = pso_state[idx] > 0.5
    entry.ci = clamp(pso_state[idx + 1], 0.0, 1.0)
    entry.sm_def = sm_def_from_pso(pso_state[idx + 2])
    entry.wiggle_iterations = clamp(round(pso_state[idx+3]), 1, MAX_WIGGLE_ITERATIONS)
    structure_list[#structure_list + 1] = entry
  end
  return structure_list
  
end

function build_model()

  -- Model defined by min_x and max_x structures
  particle_params.min_x = {}
  particle_params.max_x = {}
  particle_params.max_v = {}
  
  -- Redefine default params
  pso_params.population=5
  pso_params.neighbors=3
  pso_params.max_iterations=0
  pso_params.min_iterations=100
  pso_params.min_delta=1E-3
  pso_params.window_size=10
  pso_params.min_energy = 1E-9
  --pso_params.discount_fitness=5E-2 -- Takes too long.  All bests are from first epoch
  --pso_params.discount_fitness=5
  pso_params.discount_fitness=1.5
  
  local seg_count = structure.GetCount()

  local state_size = calculate_state_size()
  for i=1,state_size,SEGMENT_SIZE do
  
    -- First element is enable_flag
    particle_params.min_x[i] = 0.0
    particle_params.max_x[i] = 1.0
    particle_params.max_v[i] = 0.25
    
    -- Second element is CI
    particle_params.min_x[i+1] = 0.0
    particle_params.max_x[i+1] = 1.0
    particle_params.max_v[i+1] = 0.25
    
    -- Third element is sm_def
    particle_params.min_x[i+2] = 0.0
    particle_params.max_x[i+2] = 4.0
    particle_params.max_v[i+2] = 1.0
    
    -- Fourth element is wiggle_iterations
    particle_params.min_x[i+3] = 1.0
    particle_params.max_x[i+3] = MAX_WIGGLE_ITERATIONS
    particle_params.max_v[i+3] = MAX_WIGGLE_ITERATIONS / 2
  
  end
  
end

function execute_fuze_from_definition(sm_def)

SHAKE_MUTATE_DEFINITION[0] = "Disable Both"
SHAKE_MUTATE_DEFINITION[1] = "Disable Mutate"
SHAKE_MUTATE_DEFINITION[2] = "Disable Shake"
SHAKE_MUTATE_DEFINITION[3] = "Shake First"
SHAKE_MUTATE_DEFINITION[4] = "Mutate First"

  if sm_def == "Disable Both" then
    print(" --- Disable Both")
  elseif sm_def == "Disable Mutate" then
    print(" --- Disable Mutate")
    print(" ---- Shake Once")
    shake_once()
  elseif sm_def == "Disable Shake" then
    print(" --- Disable Shake")
    print(" ---- Mutate Once")
    mutate_once()
  elseif sm_def == "Shake First" then
    print(" --- Shake First")
    print(" ---- Shake Once")
    shake_once()
    print(" ---- Mutate Once")
    mutate_once()
  elseif sm_def == "Mutate First" then
    print(" --- Mutate First")
    print(" ---- Mutate Once")
    mutate_once()
    print(" ---- Shake Once")
    shake_once()
  else
    print(" --- Unknown definition: ", sm_def)
    print(" --- Warning: Skipping Shake/Mutate Order")
  end

end

function fuze_fitness_function(x, idx)

  -- Real work happens here

  save.Quickload(RESTORE_BEST_SLOT)
  local start_score = Score()
  print(" *** Particle ", idx, " ***")
  print("Start Score: ", start_score)
  
  local structure_list = get_structure_from_state(x)
  
  -- Count enabled fuze segments
  local total_enabled = 0
  for i=1,#structure_list do
    if structure_list[i].enable_flag then
      total_enabled = total_enabled + 1
    end
  end
  
  print("Total Fuze Segments: ", total_enabled)
  
  behavior.SetWigglePower('a')
  for i=1,#structure_list do
    local entry = structure_list[i]
    if entry.enable_flag then
      print(" - CI :=", entry.ci)
      behavior.SetClashImportance(entry.ci)
      execute_fuze_from_definition(entry.sm_def)
      print(" - Wiggle for ", entry.wiggle_iterations)
      structure.WiggleAll(entry.wiggle_iterations, true, true)
    end
  end
  
  print(" - Wiggle Complete")
  behavior.SetClashImportance(1.0)
  behavior.SetWigglePower('m')
  wiggle_complete()
  shake_once()
  behavior.SetWigglePower('a')
  
  local new_score = Score()
  local score_change = new_score - start_score
  
  print("End Score: ", new_score)
  print("Score Change: ", score_change)
  
  -- track when the best score increases
  if new_score >= restore_buffer_score then
    print(" -- Best score increased by "..(new_score - restore_buffer_score))
    save.Quicksave(RESTORE_BUFFER_SLOT)
    restore_buffer_score = new_score
  end

  local fitness = score_change
  print("Particle Fitness: ", fitness)

  return fitness

end

function TournamentWorstParticle()

  local score_arr = {}
  local score_sum = 0
  local score_min = 1e309
  local score_max = -1e309

  -- Get list of scores from each particle's personal best
  for i=1,#particles do
    score_arr[i] = particles[i].fitness_pb
    score_min = math.min(score_min, score_arr[i])
    score_max = math.max(score_max, score_arr[i])
  end
  
  -- Normalize scores
  for i=1,#particles do
    score_arr[i] = 1 - (score_arr[i] - score_min) / (score_max - score_min)
    score_sum = score_sum + score_arr[i]
  end
  
  for i=1,#particles do
    score_arr[i] = score_arr[i] / score_sum
  end
  
  -- Create reverse CDF from normalized scores
  local cdf = {}
  for i=1,#score_arr do
    if i == 1 then
      cdf[i] = score_arr[i]
    else
      cdf[i] = score_arr[i] + cdf[i-1]
    end
  end
  
  for i=1,#cdf do
    cdf[i] = cdf[i]
    print("Particle("..i.."), FitnessPB("..particles[i].fitness_pb.."): CDF("..cdf[i]..")")
  end
  
  -- get value between [0.0, 1.0]
  local random_select = math.random()
  local select_idx = -1
  
  for i=1,#cdf do
    if random_select <= cdf[i] then
      select_idx = i
      break
    end
  end
  
  if select_idx > 0 and select_idx <= #particles then
    print("Weakest link selected: Particle ", select_idx)
    RandomizeParticleState(select_idx)
  else
    print("Unable to randomize particle, index out of scope: ", select_idx)
  end

end

function fuze_terminate_function(x)
  -- Repurpose terminate function to run end-of-epoch work
  print("Evaluate epoch performance")
  
  if restore_buffer_score > restore_score then
    print("Updating new restore best")
    save.Quickload(RESTORE_BUFFER_SLOT)
    restore_buffer_score = Score()
    local score_change = restore_buffer_score - restore_score
    print(" -- New Score: ", restore_buffer_score)
    print(" -- Score Change: ", score_change)
    restore_score = restore_buffer_score
    save.Quicksave(RESTORE_BEST_SLOT)
    tournament_idx = 0
  else
  
    tournament_idx = tournament_idx + 1
    if tournament_idx > BAD_GENERATION_EPOCHS then
      TournamentWorstParticle()
      tournament_idx = 0
    end
  end
  
  -- No reason to terminate yet
  return false
end

function execute_recipe_pso()

  local start_score = Score()
  tournament_idx = 0

  save.Quicksave(RESTORE_BEST_SLOT)
  save.Quicksave(RESTORE_BUFFER_SLOT)
  restore_score = Score()
  restore_buffer_score = Score()

  -- *** Let the real work begin! *** --
  solution = PSO(fuze_fitness_function, fuze_terminate_function)
  -- ** End Heavy Work ** --
  
  print("Finishing up...")
  save.Quickload(RESTORE_BEST_SLOT)
  local final_fitness = band_fitness_function(solution)
  
  print("Final Wiggle")
  behavior.SetWigglePower('m')
  wiggle_complete()
  behavior.SetWigglePower('a')
  
  local final_score = Score()
  
  print("Start Score: ", start_score)
  print("Final Score: ", final_score)
  print("Score Change: ", final_score - start_score)
  
end

function main()
  --Test()
  
  print("Running recipe:    ", recipe_name)
  print("Version:           ", recipe_version)
  print("Brought to you by: ", recipe_author)
  
  build_model()
  
  while true do
    execute_recipe_pso()
    print("Restarting with new particles")
  end
  
end

local function _error_(e)
    save.Quickload(RESTORE_BEST_SLOT)
    print("Error: ", e)
    return e
end

xpcall(main, _error_)
